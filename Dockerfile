FROM rocker/verse:latest

RUN R -e "install.packages('gapminder', repos = 'http://cran.us.r-project.org')"
RUN R -e "install.packages('palmerpenguins', repos = 'http://cran.us.r-project.org')"
RUN R -e "install.packages('hexbin', repos = 'http://cran.us.r-project.org')"
RUN R -e "install.packages('GGally', repos = 'http://cran.us.r-project.org')"
RUN R -e "install.packages('learnr', repos = 'http://cran.us.r-project.org')"
RUN R -e "install.packages('rmarkdown')"

ADD day1 /home/rstudio/

# App on port 6879
EXPOSE 6879

# Create unprivileged user
RUN useradd -m learnr -d /home/learnr

# Run as user learnr to avoid running as root
USER learnr

CMD ["R", "-e", "rmarkdown::run('/home/rstudio/day1-Tutorial.Rmd', shiny_args=list(host = '0.0.0.0', port = 6879))"]